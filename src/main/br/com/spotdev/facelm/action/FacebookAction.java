/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.action;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import br.com.spotdev.facelm.facebookconnection.FacebookConnection;

/**
 * Superclasse para representar a realização de ações
 * dentro do facebook
 * 
 * @author Davi Salles
 */
public abstract class FacebookAction {
	
	/**
	 * Conexão que estará realizando a ação
	 */
	private FacebookConnection facebookConnection = null;
	
	/**
	 * Função abstrata para realizar a ação
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	public abstract void doAction() throws ClientProtocolException, IOException;

	/**
	 * @return facebookCoonnection retorna a instância da conexão
	 */
	public FacebookConnection getFacebookConnection() {
		return facebookConnection;
	}

	/**
	 * Define a instância da conexão com o facebook
	 * @param facebookConnection instância da conexão
	 */
	public void setFacebookConnection(FacebookConnection facebookConnection) {
		this.facebookConnection = facebookConnection;
	}

}
