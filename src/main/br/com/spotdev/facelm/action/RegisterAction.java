/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.action;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import br.com.spotdev.facelm.dao.FacebookAccount;
import br.com.spotdev.facelm.facebookconnection.FacebookConnection;
import br.com.spotdev.facelm.facebookconnection.FacebookURL;
import br.com.spotdev.facelm.facebookconnection.apache.FacebookHttpClientBuilder;
import br.com.spotdev.facelm.facebookconnection.apache.FacebookHttpContext;
import br.com.spotdev.facelm.facebookconnection.apache.FacebookHttpPost;
import br.com.spotdev.facelm.facebookconnection.apache.FacebookRequestConfig;

/**
 * Subclasse de Facebook action, utilizada para realizar
 * a ação de cadastro no facebook
 * 
 * @author Davi Salles
 */
public class RegisterAction extends FacebookAction {


	@Override
	public void doAction() throws ClientProtocolException, IOException {
		
		FacebookConnection connection = getFacebookConnection();
		FacebookAccount facebookAccount = connection.getFacebookAccount();
		
		// Cria o HttpClient e context
		RequestConfig requestConfig = FacebookRequestConfig.custom().setConnectTimeout(5000).build();
		HttpClient client = FacebookHttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
		FacebookHttpContext httpContext = FacebookHttpContext.create();
		
		connection.setHttpContext(httpContext);
		connection.setHttpClient(client);
		
		// Cria a requisição para adquirir alguns cookies e parametros
		HttpGet httpGet = new HttpGet(FacebookURL.URL_REGISTER_FORM);
		HttpResponse response = client.execute(httpGet,httpContext);
		String registerFormHTML = EntityUtils.toString(response.getEntity(), "UTF-8");
		
		// Adquiri algun parâmetros do form da requisição GET
		Document registerFormDocument = Jsoup.parse(registerFormHTML);
		Element registerForm = registerFormDocument.getElementById("reg");
		Elements hiddenValues = registerForm.getElementsByAttributeValue("type", "hidden");
		
		// Cria a requisição post para efetuar o cadastro
		FacebookHttpPost post = new FacebookHttpPost(FacebookURL.URL_REGISTER);	
		
		// Adquiri o valor do cookie datr
		Elements reg_instance_elements = registerForm.getElementsByAttributeValue("name", "reg_instance");
		String reg_instance_value = reg_instance_elements.attr("value");
		
		// Adiciona alguns cookies padrões para o cadastro
		httpContext.addCookie("_js_datr", reg_instance_value);
		httpContext.addCookie("_js_reg_fb_gate", "https%3A%2F%2Fwww.facebook.com%2Fr.php");
		httpContext.addCookie("_js_reg_fb_ref", "https%3A%2F%2Fwww.facebook.com%2Fr.php");
		httpContext.addCookie("datr", reg_instance_value);
		httpContext.addCookie("reg_fb_gate", "https%3A%2F%2Fwww.facebook.com%2Fr.php");
		httpContext.addCookie("reg_fb_ref", "https%3A%2F%2Fwww.facebook.com%2Fr.php");
		httpContext.addCookie("wd", "1360x320");
		
		// Adiciona cabeçalhos padrões para o cadastro
		post.addHeader("content-type", "application/x-www-form-urlencoded");
		post.addHeader("accept", "*/*");
		post.addHeader("referer", "https://www.facebook.com/r.php?locale=pt_BR");
		post.addHeader("accept-encoding", "gzip, deflate, br");
		post.addHeader("accept-language", "pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4");
		
		// Pega a data de nascimento da conta em gregorian calendar
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(facebookAccount.getBirthDate());
		
		// Adiciona parametros da conta que será criada para o cadastro
		post.addParameter("firstname", facebookAccount.getFirstName());
		post.addParameter("lastname", facebookAccount.getLastName());
		post.addParameter("reg_email__", facebookAccount.getEmail());
		post.addParameter("reg_email_confirmation__", facebookAccount.getEmail());
		post.addParameter("reg_passwd__", facebookAccount.getPassword());
		post.addParameter("sex", facebookAccount.getGender().getId()+"");
		post.addParameter("birthday_day", gc.get(GregorianCalendar.DAY_OF_MONTH)+"");
		post.addParameter("birthday_month", (gc.get(GregorianCalendar.MONTH)+1)+"");
		post.addParameter("birthday_year", gc.get(GregorianCalendar.YEAR)+"");
		post.addParameter("ignore", "captcha");
		post.addParameter("__a", "1");
		
		// Adiciona os parâmetros hiddens da primeira requisição GET
		List<Element> elementsHidden = hiddenValues.subList(0, hiddenValues.size());
		for(Element hidden : elementsHidden){
			post.addParameter(hidden.attr("name"), hidden.attr("value"));
		}
		
		httpContext.getCookieStore().getCookies().forEach((value)->System.out.println(value));
		
		// Constrói os parâmetros e efetua o cadastro
		post.buildParameters();
		response = client.execute(post, httpContext);
	}



}
