/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.action;

/**
 * Description of MessageAction.
 * 
 * @author Davi Salles
 */
public abstract class MessageAction extends FacebookAction {
	// Start of user code (user defined attributes for MessageAction)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public MessageAction() {
		// Start of user code constructor for MessageAction)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for MessageAction)
	
	// End of user code


}
