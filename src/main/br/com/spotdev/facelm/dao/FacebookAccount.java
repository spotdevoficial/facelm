/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.dao;

import java.util.Date;

/**
 * Description of FacebookAccount.
 * 
 * @author Davi Salles
 */
public class FacebookAccount {
	/**
	 * Description of the property gender.
	 */
	private Gender gender = null;
	
	/**
	 * Description of the property name.
	 */
	private String firstName = null;
	
	/**
	 * Description of the property name.
	 */
	private String lastName = null;
	
	/**
	 * Description of the property birthDate.
	 */
	private Date birthDate = new Date();
	
	/**
	 * Description of the property password.
	 */
	private String password = null;
	
	/**
	 * Description of the property email.
	 */
	private String email = null;
	
	
	/**
	 * The constructor.
	 */
	public FacebookAccount() {
		super();
	}
	

	/**
	 * Returns gender.
	 * @return gender 
	 */
	public Gender getGender() {
		return this.gender;
	}
	
	/**
	 * Sets a value to attribute gender. 
	 * @param newGender 
	 */
	public void setGender(Gender newGender) {
	    this.gender = newGender;
	}

	
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns name.
	 * @return name 
	 */
	public String getFirstName() {
		return this.firstName;
	}
	
	/**
	 * Sets a value to attribute name. 
	 * @param newName 
	 */
	public void setFirstName(String newName) {
	    this.firstName = newName;
	}

	/**
	 * Returns birthDate.
	 * @return birthDate 
	 */
	public Date getBirthDate() {
		return this.birthDate;
	}
	
	/**
	 * Sets a value to attribute birthDate. 
	 * @param newBirthDate 
	 */
	public void setBirthDate(Date newBirthDate) {
	    this.birthDate = newBirthDate;
	}

	/**
	 * Returns password.
	 * @return password 
	 */
	public String getPassword() {
		return this.password;
	}
	
	/**
	 * Sets a value to attribute password. 
	 * @param newPassword 
	 */
	public void setPassword(String newPassword) {
	    this.password = newPassword;
	}

	/**
	 * Returns email.
	 * @return email 
	 */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * Sets a value to attribute email. 
	 * @param newEmail 
	 */
	public void setEmail(String newEmail) {
	    this.email = newEmail;
	}



}
