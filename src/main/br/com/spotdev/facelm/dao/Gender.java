/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.dao;

/**
 * Enum para gender do FacebookAccount
 * 
 * @author Davi Salles
 */
public enum Gender {

    MALE(2),		// Gênero masculino
    FEMALE(1);		// Gênero feminino
    
	private int id;	// ID definido pelo protocolo do facebook
	
    Gender(int id){
    	this.id = id;
    }

    /**
     * Função getter do id
     * @return Retorna o id do enum
     */
	public int getId() {
		return id;
	}
    
}
