/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.dao;

// Start of user code (user defined imports)

// End of user code

/**
 * Description of Message.
 * 
 * @author Davi Salles
 */
public class Message {
	/**
	 * Description of the property messageType.
	 */
	private MessageType messageType = null;
	
	/**
	 * Description of the property messageValue.
	 */
	private String messageValue = "";
	
	// Start of user code (user defined attributes for Message)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Message() {
		// Start of user code constructor for Message)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Message)
	
	// End of user code
	/**
	 * Returns messageType.
	 * @return messageType 
	 */
	public MessageType getMessageType() {
		return this.messageType;
	}
	
	/**
	 * Sets a value to attribute messageType. 
	 * @param newMessageType 
	 */
	public void setMessageType(MessageType newMessageType) {
	    this.messageType = newMessageType;
	}

	/**
	 * Returns messageValue.
	 * @return messageValue 
	 */
	public String getMessageValue() {
		return this.messageValue;
	}
	
	/**
	 * Sets a value to attribute messageValue. 
	 * @param newMessageValue 
	 */
	public void setMessageValue(String newMessageValue) {
	    this.messageValue = newMessageValue;
	}



}
