/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.dao;

// Start of user code (user defined imports)

// End of user code

/**
 * Description of MessageType.
 * 
 * @author Davi Salles
 */
public enum MessageType {
    /**
     * Description of IMAGE.
     */
    IMAGE,
    
    /**
     * Description of TEXT.
     */
    TEXT,
    
	// Start of user code (user defined enum literals for MessageType)
	
	// End of user code
}
