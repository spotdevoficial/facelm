/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.dao;

import java.util.Date;
// Start of user code (user defined imports)
import java.util.HashSet;

// End of user code

/**
 * Description of Campaign.
 * 
 * @author Davi Salles
 */
public class Campaign {
	/**
	 * Description of the property startTime.
	 */
	private Date startTime = new Date();
	
	/**
	 * Description of the property messages.
	 */
	public HashSet<Message> messages = new HashSet<Message>();
	
	/**
	 * Description of the property message.
	 */
	private Message message = null;
	
	// Start of user code (user defined attributes for Campaign)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Campaign() {
		// Start of user code constructor for Campaign)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Campaign)
	
	// End of user code
	/**
	 * Returns startTime.
	 * @return startTime 
	 */
	public Date getStartTime() {
		return this.startTime;
	}
	
	/**
	 * Sets a value to attribute startTime. 
	 * @param newStartTime 
	 */
	public void setStartTime(Date newStartTime) {
	    this.startTime = newStartTime;
	}

	/**
	 * Returns messages.
	 * @return messages 
	 */
	public HashSet<Message> getMessages() {
		return this.messages;
	}

	/**
	 * Returns message.
	 * @return message 
	 */
	public Message getMessage() {
		return this.message;
	}
	
	/**
	 * Sets a value to attribute message. 
	 * @param newMessage 
	 */
	public void setMessage(Message newMessage) {
	    this.message = newMessage;
	}



}
