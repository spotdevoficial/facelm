/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.generation;

// Start of user code (user defined imports)

// End of user code

/**
 * Description of Generator.
 * 
 * @author Davi Salles
 */
public interface Generator {
	// Start of user code (user defined attributes for Generator)
	
	// End of user code
	
	/**
	 * Description of the method getValue.
	 */
	public void getValue();
	
	// Start of user code (user defined methods for Generator)
	
	// End of user code


}
