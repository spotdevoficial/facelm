/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.campaign;

import br.com.spotdev.facelm.dao.Campaign;
import br.com.spotdev.facelm.dao.Message;
import br.com.spotdev.facelm.events.FaceActionEvent;

public class PostCampaignManager extends CampaignManager {
	// Start of user code (user defined attributes for PostCampaignManager)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public PostCampaignManager() {
		// Start of user code constructor for PostCampaignManager)
		super();
		// End of user code
	}
	
	/**
	 * Description of the method start.
	 * @param message 
	 */
	public void start(Message message) {
		// Start of user code for method start
		// End of user code
	}
	 
	/**
	 * Description of the method load.
	 * @param campaign 
	 */
	public void load(Campaign campaign) {
		// Start of user code for method load
		// End of user code
	}
	 
	/**
	 * Description of the method stop.
	 */
	public void stop() {
		// Start of user code for method stop
		// End of user code
	}
	 
	/**
	 * Description of the method addCampaignEvent.
	 * @param facebookEvent 
	 */
	public void addCampaignEvent(FaceActionEvent facebookEvent) {
		// Start of user code for method addCampaignEvent
		// End of user code
	}

	 
	// Start of user code (user defined methods for PostCampaignManager)
	
	// End of user code


}
