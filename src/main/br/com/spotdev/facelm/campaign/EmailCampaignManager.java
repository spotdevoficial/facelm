/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.campaign;

import br.com.spotdev.facelm.dao.Campaign;
import br.com.spotdev.facelm.dao.Message;
import br.com.spotdev.facelm.events.FaceActionEvent;
// Start of user code (user defined imports)

// End of user code

/**
 * Description of EmailCampaignManager.
 * 
 * @author Davi Salles
 */
public class EmailCampaignManager extends CampaignManager {
	// Start of user code (user defined attributes for EmailCampaignManager)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public EmailCampaignManager() {
		// Start of user code constructor for EmailCampaignManager)
		super();
		// End of user code
	}
	 
	/**
	 * Description of the method load.
	 * @param campaign 
	 */
	public void load(Campaign campaign) {
		// Start of user code for method load
		// End of user code
	}
	 
	/**
	 * Description of the method stop.
	 */
	public void stop() {
		// Start of user code for method stop
		// End of user code
	}
	 
	/**
	 * Description of the method addCampaignEvent.
	 * @param facebookEvent 
	 */
	public void addCampaignEvent(FaceActionEvent facebookEvent) {
		// Start of user code for method addCampaignEvent
		// End of user code
	}

	@Override
	public void start(Message message) {
		// TODO Auto-generated method stub
		
	}
	 
	// Start of user code (user defined methods for EmailCampaignManager)
	
	// End of user code


}
