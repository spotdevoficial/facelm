/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.campaign;

import br.com.spotdev.facelm.dao.Campaign;
import br.com.spotdev.facelm.dao.Message;
import br.com.spotdev.facelm.events.FaceActionEvent;
// Start of user code (user defined imports)

// End of user code

/**
 * Description of ChatMessageCampaignManager.
 * 
 * @author Davi Salles
 */
public class ChatMessageCampaignManager extends CampaignManager {
	// Start of user code (user defined attributes for ChatMessageCampaignManager)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public ChatMessageCampaignManager() {
		// Start of user code constructor for ChatMessageCampaignManager)
		super();
		// End of user code
	}
	
	/**
	 * Description of the method load.
	 * @param campaign 
	 */
	public void load(Campaign campaign) {
		// Start of user code for method load
		// End of user code
	}
	 
	/**
	 * Description of the method stop.
	 */
	public void stop() {
		// Start of user code for method stop
		// End of user code
	}
	 
	/**
	 * Description of the method addCampaignEvent.
	 * @param facebookEvent 
	 */
	public void addCampaignEvent(FaceActionEvent facebookEvent) {
		// Start of user code for method addCampaignEvent
		// End of user code
	}

	@Override
	public void start(Message message) {
		// TODO Auto-generated method stub
		
	}
	 
	// Start of user code (user defined methods for ChatMessageCampaignManager)
	
	// End of user code


}
