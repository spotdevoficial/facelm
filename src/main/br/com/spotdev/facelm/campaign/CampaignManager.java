/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.campaign;

import java.util.HashSet;

import br.com.spotdev.facelm.dao.Campaign;
import br.com.spotdev.facelm.dao.Message;
import br.com.spotdev.facelm.events.FaceActionEvent;
import br.com.spotdev.facelm.facebookconnection.FacebookConnection;

/**
 * Description of CampaignManager.
 * 
 * @author Davi Salles
 */
public abstract class CampaignManager {
	/**
	 * Description of the property facebookConnections.
	 */
	public HashSet<FacebookConnection> facebookConnections = new HashSet<FacebookConnection>();
	
	/**
	 * Description of the property campanha.
	 */
	private Campaign campanha = null;
	
	// Start of user code (user defined attributes for CampaignManager)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public CampaignManager() {
		// Start of user code constructor for CampaignManager)
		super();
		// End of user code
	}
	
	/**
	 * Description of the method start.
	 * @param message 
	 */
	public abstract void start(Message message);
	 
	/**
	 * Description of the method load.
	 * @param campaign 
	 */
	public abstract void load(Campaign campaign);
	 
	/**
	 * Description of the method stop.
	 */
	public abstract void stop();
	 
	/**
	 * Description of the method addCampaignEvent.
	 * @param facebookEvent 
	 */
	public abstract void addCampaignEvent(FaceActionEvent facebookEvent);
	 
	// Start of user code (user defined methods for CampaignManager)
	
	// End of user code
	/**
	 * Returns facebookConnections.
	 * @return facebookConnections 
	 */
	public HashSet<FacebookConnection> getFacebookConnections() {
		return this.facebookConnections;
	}

	/**
	 * Returns campanha.
	 * @return campanha 
	 */
	public Campaign getCampanha() {
		return this.campanha;
	}
	
	/**
	 * Sets a value to attribute campanha. 
	 * @param newCampanha 
	 */
	public void setCampanha(Campaign newCampanha) {
	    this.campanha = newCampanha;
	}



}
