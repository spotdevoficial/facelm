package br.com.spotdev.facelm.facebookconnection.apache;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;

import br.com.spotdev.facelm.FacebookLM;

public class FacebookRequestConfig extends RequestConfig {

	public static Builder custom(){
		Builder builder = RequestConfig.custom();
		if(FacebookLM.CHARLES_PROXY)
			builder.setProxy(new HttpHost("127.0.0.1",8888,"http"));
		
		return builder;
	}
	
}
