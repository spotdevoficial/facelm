package br.com.spotdev.facelm.facebookconnection.apache;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;

import br.com.spotdev.facelm.FacebookLM;

/**
 * Subclasse do HttpClientBuilder para adicionar proxy do charles
 * 
 * @author Davi Salles
 */
public class FacebookHttpClientBuilder extends HttpClientBuilder {

	/**
	 * Função para utilizar o charles proxy para debug de HttpRequest
	 * se ativado
	 */
	public static FacebookHttpClientBuilder create() {
		
		FacebookHttpClientBuilder clientBuilder = new FacebookHttpClientBuilder();
		
		if(FacebookLM.CHARLES_PROXY){
			SSLContext sslcontext = null;
			try {
				sslcontext = SSLContexts.custom().loadTrustMaterial(new File("charles_cert.p12"), "bigarismo".toCharArray(),
						new TrustSelfSignedStrategy()).build();
			} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Allow TLSv1 protocol only
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1" }, null,
					SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			clientBuilder.setSSLSocketFactory(sslsf);
		}
		return clientBuilder;
	}
}
