package br.com.spotdev.facelm.facebookconnection.apache;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

/**
 * @Descrição Classe que extende o HttpPost para adicionar
 * os headers padrões para acessar o facebook
 * 
 * @Autor Davi Salles
 */

public class FacebookHttpPost extends HttpPost {

	/**
	 * Lista de parâmetros utilizados no POST
	 */
	private final List<NameValuePair> parameters = new ArrayList <NameValuePair>();
	
	/**
	 * Construtor principal
	 * @param url em que o post será executado
	 */
	public FacebookHttpPost(String url){
		super(url);
		
		// Adiciona headers padrões para o facebook
		this.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.7 Safari/537.36");
		this.addHeader("Origin","https://www.facebook.com");
	}
	
	/**
	 * Adiciona um parâmetro post
	 * @param key Nome do parâmetro
	 * @param value Valor do parâmetro
	 */
	public void addParameter(String key, String value){
		parameters.add(new BasicNameValuePair(key, value));
	}

	/**
	 * Retorna a lista de parâmetros do post
	 * @return Lista de parâmetros
	 */
	public List<NameValuePair> getParameters() {
		return parameters;
	}
	
	/**
	 * Da encode nos parâmetros já adicionados e adiciona à SuperClasse
	 */
	public void buildParameters(){
		try {
			super.setEntity(new UrlEncodedFormEntity(parameters,"UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
}
