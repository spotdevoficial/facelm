package br.com.spotdev.facelm.facebookconnection.apache;

import java.util.GregorianCalendar;

import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

/**
 * @Descrição Classe que extende o HttpClientContext para
 * adicionar funções melhoradas
 * 
 * @Autor Davi Salles
 * @Data 14/04/2016
 */

public class FacebookHttpContext extends HttpClientContext {

	public FacebookHttpContext(final HttpContext context){
		super(context);
	}
	
	/**
	 * Função para verificar se um cookie específico existe
	 * no HttpClientContext
	 * 
	 * @param cookie buscado
	 * @return se o cookie existe ou não
	 */
	public boolean hasCookie(String cookie){
		CookieStore cookies = this.getCookieStore();
	    for(Cookie c : cookies.getCookies()){
	    	if(c.getName().equals(cookie)){
	    		return true;
	    	}
	    }
	    
	    return false;
	}
	
	/**
	 * Adiciona um cookie ao context para o facebook
	 * 
	 * @param name Nome do cookie
	 * @param value Valor do cookie
	 */
	public void addCookie(String name, String value){
		BasicClientCookie cookie = new BasicClientCookie(name, value);
		cookie.setDomain(".facebook.com");
	    cookie.setPath("/");
	    GregorianCalendar gc = new GregorianCalendar();
	    gc.add(GregorianCalendar.MONTH, 3);
	    cookie.setExpiryDate(gc.getTime());
		this.getCookieStore().addCookie(cookie);

	}
	
	/**
	 * Função estática para criar uma instância do facebooHttpContext
	 */
	public static FacebookHttpContext create(){
		return new FacebookHttpContext(new BasicHttpContext());
	}
	
}
