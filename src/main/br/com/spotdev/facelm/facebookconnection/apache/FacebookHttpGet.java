package br.com.spotdev.facelm.facebookconnection.apache;

import org.apache.http.client.methods.HttpGet;

/**
 * @Descrição Classe que extende o HttpGet para adicionar
 * os headers padrões para acessar o facebook
 * 
 * @Autor Davi Salles
 */

public class FacebookHttpGet extends HttpGet {

	/**
	 * Construtor principal
	 * @param url em que o post será executado
	 */
	public FacebookHttpGet(String url){
		super(url);
		
		// Adiciona headers padrões para o facebook
		this.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.7 Safari/537.36");
		this.addHeader("Origin","https://www.facebook.com");
		this.addHeader("cookie","reg_fb_gate=https%3A%2F%2Fwww.facebook.com%2F;");
	}
	
}
