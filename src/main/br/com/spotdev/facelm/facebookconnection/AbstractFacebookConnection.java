/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.facebookconnection;

import org.apache.http.client.HttpClient;

import br.com.spotdev.facelm.dao.FacebookAccount;
import br.com.spotdev.facelm.facebookconnection.apache.FacebookHttpContext;

/**
 * SuperClasse da FacebookConnection, utilizada para armazenar 
 * a informação da subclasse
 * 
 * @author Davi Salles
 */
abstract class AbstractFacebookConnection {
	
	/**
	 * Conta do facebook ligada a conexão
	 */
	private FacebookAccount facebookAccount = null;
	
	/**
	 * HttpClient utilizado para a conexão
	 */
	private HttpClient httpClient = null;
	
	/**
	 * HttpContext utilizado para a conexão
	 */
	private FacebookHttpContext httpContext = null;
	
	/**
	 * Returns facebookAccount.
	 * @return facebookAccount 
	 */
	public FacebookAccount getFacebookAccount() {
		return this.facebookAccount;
	}
	
	/**
	 * Sets a value to attribute facebookAccount. 
	 * @param newFacebookAccount 
	 */
	public void setFacebookAccount(FacebookAccount newFacebookAccount) {
	    this.facebookAccount = newFacebookAccount;
	}

	/**
	 * Returns httpClient.
	 * @return httpClient 
	 */
	public HttpClient getHttpClient() {
		return this.httpClient;
	}
	
	/**
	 * Sets a value to attribute httpClient. 
	 * @param newHttpClient 
	 */
	public void setHttpClient(HttpClient newHttpClient) {
	    this.httpClient = newHttpClient;
	}

	/**
	 * Returns httpContext.
	 * @return httpContext 
	 */
	public FacebookHttpContext getHttpContext() {
		return this.httpContext;
	}
	
	/**
	 * Sets a value to attribute httpContext. 
	 * @param newHttpContext 
	 */
	public void setHttpContext(FacebookHttpContext newHttpContext) {
	    this.httpContext = newHttpContext;
	}



}
