/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.facebookconnection;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.impl.client.HttpClients;
import br.com.spotdev.facelm.action.FacebookAction;
import br.com.spotdev.facelm.dao.FacebookAccount;
import br.com.spotdev.facelm.facebookconnection.apache.FacebookHttpContext;
import br.com.spotdev.facelm.facebookconnection.apache.FacebookHttpPost;

/**
 * Classe utilizada para gerenciar a conexão com o facebook
 * 
 * @author Davi Salles
 */
public class FacebookConnection extends AbstractFacebookConnection {
	
	/**
	 * Construtor principal
	 * @param facebookAccount Conta do facebook que estará ligada a conexão
	 */
	public FacebookConnection(FacebookAccount facebookAccount) {
		super.setFacebookAccount(facebookAccount);
	}
	
	/**
	 * Realiza login com essa instância da conexão
	 */
	public boolean login() throws IOException {
		
		FacebookAccount account = getFacebookAccount();
		
		// Instancia as conexões
		super.setHttpClient(HttpClients.createDefault());
		super.setHttpContext(FacebookHttpContext.create());
		
		// Cria e executa uma equisição POST 
		FacebookHttpPost httpPost = new FacebookHttpPost(FacebookURL.URL_LOGIN);
		httpPost.addParameter("email", account.getEmail());
		httpPost.addParameter("pass", account.getPassword());
		httpPost.buildParameters();
		getHttpClient().execute(httpPost, getHttpContext());
	
		return getHttpContext().hasCookie("c_user");
		
	}

	/**
	 * Realiza uma facebookAction nessa instância da conexão
	 * @param facebookAction ação que será realizada
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	public void doFacebookAction(FacebookAction facebookAction) throws ClientProtocolException, IOException {
		facebookAction.setFacebookConnection(this);
		facebookAction.doAction();
	}



}
