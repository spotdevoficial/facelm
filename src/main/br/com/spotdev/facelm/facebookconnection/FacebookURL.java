package br.com.spotdev.facelm.facebookconnection;

public abstract class FacebookURL {
	
	public static final String URL_LOGIN = "https://www.facebook.com/login.php?login_attempt=1&lwv=111";
	public static final String URL_CHAT = "https://www3.Facebook.com.br/chats";
	public static final String URL_MESSAGE = "https://www3.Facebook.com.br/chats/%s/messages";
	public static final String URL_REGISTER_FORM = "https://www.facebook.com/r.php";
	public static final String URL_REGISTER = "https://www.facebook.com/ajax/register.php?dpr=1";

}
