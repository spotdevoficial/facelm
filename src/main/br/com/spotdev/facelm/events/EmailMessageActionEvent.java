/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.events;

// Start of user code (user defined imports)

// End of user code

/**
 * Description of EmailMessageActionEvent.
 * 
 * @author Davi Salles
 */
public interface EmailMessageActionEvent  extends FaceActionEvent{
	// Start of user code (user defined attributes for EmailMessageActionEvent)
	
	// End of user code
	
	// Start of user code (user defined methods for EmailMessageActionEvent)
	
	// End of user code


}
