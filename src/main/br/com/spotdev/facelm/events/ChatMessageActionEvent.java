/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.events;

// Start of user code (user defined imports)

// End of user code

/**
 * Description of ChatMessageActionEvent.
 * 
 * @author Davi Salles
 */
public interface ChatMessageActionEvent  extends FaceActionEvent{
	// Start of user code (user defined attributes for ChatMessageActionEvent)
	
	// End of user code
	
	// Start of user code (user defined methods for ChatMessageActionEvent)
	
	// End of user code


}
