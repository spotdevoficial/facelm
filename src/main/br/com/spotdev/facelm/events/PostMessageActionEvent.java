/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.events;

// Start of user code (user defined imports)

// End of user code

/**
 * Description of PostMessageActionEvent.
 * 
 * @author Davi Salles
 */
public interface PostMessageActionEvent  extends FaceActionEvent{
	// Start of user code (user defined attributes for PostMessageActionEvent)
	
	// End of user code
	
	// Start of user code (user defined methods for PostMessageActionEvent)
	
	// End of user code


}
