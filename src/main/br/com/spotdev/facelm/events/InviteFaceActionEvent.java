/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package br.com.spotdev.facelm.events;

// Start of user code (user defined imports)

// End of user code

/**
 * Description of InviteFaceActionEvent.
 * 
 * @author Davi Salles
 */
public interface InviteFaceActionEvent  extends FaceActionEvent{
	// Start of user code (user defined attributes for InviteFaceActionEvent)
	
	// End of user code
	
	// Start of user code (user defined methods for InviteFaceActionEvent)
	
	// End of user code


}
