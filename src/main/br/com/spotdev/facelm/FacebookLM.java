package br.com.spotdev.facelm;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.http.client.ClientProtocolException;

import br.com.spotdev.facelm.action.RegisterAction;
import br.com.spotdev.facelm.dao.FacebookAccount;
import br.com.spotdev.facelm.dao.Gender;
import br.com.spotdev.facelm.facebookconnection.FacebookConnection;

/*******************************************************************************
 * 2016, All rights reserved. *******************************************************************************/

/**
 * Classe principal do sistema
 * 
 * @author Davi Salles
 */
public class FacebookLM {
	
	public static final boolean CHARLES_PROXY = true;
	
	public static void main(String[] args) throws ClientProtocolException, IOException, ParseException{
		
		FacebookAccount fc = new FacebookAccount();
		fc.setFirstName("Pedro");
		fc.setLastName("Guimarães");
		fc.setGender(Gender.MALE);
		fc.setEmail("pedraes@zoho.com");
		fc.setPassword("bigarismo97");
		fc.setBirthDate(new SimpleDateFormat("dd/MM/yyyy").parse("07/07/1997"));
		
		FacebookConnection facebookConnection = new FacebookConnection(fc);
		RegisterAction registerAction = new RegisterAction();
		facebookConnection.doFacebookAction(registerAction);
	}


}
